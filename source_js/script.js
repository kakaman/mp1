$(document).ready(function(){
    //smooth scrolling
    $(function() {
        $("nav a").click(function() {
            var target = $(this.hash);
            if (target.length) {
                $('html,body').animate({ scrollTop: target.offset().top-77 }, 1000);
                return false;
            }
        });
    });

//modal
    $(function() {
        $('.open-modal').on('click', function(e) {
            e.preventDefault();
            var image = $(this).attr('href');
            $('html').addClass('no-scroll');
            $('body').append('<div class="modal"><img src="' + image + '">');
        });

        $('body').on('click', '.modal', function() {
            $('html').removeClass('no-scroll');
            $('.modal').remove();
        });
    });


// Position indicator
    var $home = $("#home");
    var homepos = $home.offset().top;

    var $wikipedia = $("#wikipedia");
    var wikipediapos = $wikipedia.offset().top;

    var $theme = $("#theme");
    var themepos = $theme.offset().top;

    var $glimpse = $("#glimpse");
    var glimpsepos = $glimpse.offset().top;

    var $icons = $("#icons");
    var iconspos = $icons.offset().top;

    var $homenav = $("#l_1");
    var $wikipedianav = $("#l_2");
    var $themenav = $("#l_3");
    var $glimpsenav = $("#l_4");
    var $iconnav = $("#l_5");

    var $activeClass = $homenav;

    $(window).scroll(function(){
        var scrollPos = $(document).scrollTop() + 80;
        var winHeight = $(window).height();
        var docHeight = $(document).height();
        if(($(document).scrollTop()+winHeight) == docHeight) {
            $activeClass.removeClass("active-nav");
            $activeClass = $iconnav;
            $activeClass.addClass("active-nav");
        }
        else if(scrollPos < wikipediapos){
            $activeClass.removeClass("active-nav");
            $activeClass = $homenav;
            $activeClass.addClass("active-nav");
        }
        else if((scrollPos > wikipediapos) && (scrollPos < themepos)){
            $activeClass.removeClass("active-nav");
            $activeClass = $wikipedianav;
            $activeClass.addClass("active-nav");
        }
        else if((scrollPos > themepos) && (scrollPos < glimpsepos)){
            $activeClass.removeClass("active-nav");
            $activeClass = $themenav;
            $activeClass.addClass("active-nav");
        }
        else if((scrollPos > glimpsepos) && (scrollPos < iconspos)){
            $activeClass.removeClass("active-nav");
            $activeClass = $glimpsenav;
            $activeClass.addClass("active-nav");
        }
        else if(scrollPos > iconspos){
            $activeClass.removeClass("active-nav");
            $activeClass = $iconnav;
            $activeClass.addClass("active-nav");
        }
        else {
            $activeClass.addClass("active-nav")
        }
    });

});

// Resize Nav Bar
$(document).on("scroll",function(){
    if($(document).scrollTop()>100){
        $("header").removeClass("large").addClass("small");
    } else{
        $("header").removeClass("small").addClass("large");
    }
});

// Carousel
$(function() {
    function changeSlide( newSlide ) {
        currSlide = newSlide;

        if ( currSlide > maxSlide ) currSlide = 0;
        else if ( currSlide < 0 ) currSlide = maxSlide;

        // animate the slide reel
        $slides.animate({ left : currSlide * -$(window).width() }, 400, 'swing', function() {
            if ( currSlide == 0 ) $leftnav.hide();
            else $leftnav.show();

            if ( currSlide == maxSlide ) $rightnav.hide();
            else $rightnav.show();
        });
    }

    function nextSlide() {
        changeSlide( currSlide + 1 );
    }

    var currSlide = 0,
        $carousel = $('#carousel'),
        $slides = $carousel.find('#slides'),
        maxSlide = $slides.children().length - 1,
        $leftnav = $carousel.find('#left-button'),
        $rightnav = $carousel.find('#right-button');

    // left arrow
    $leftnav.click(function(ev) {
        ev.preventDefault();
        changeSlide( currSlide - 1 );
    });

    // right arrow
    $rightnav.click(function(ev) {
        ev.preventDefault();
        changeSlide( currSlide + 1 );
    });

});
